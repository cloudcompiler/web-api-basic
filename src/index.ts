import * as express from 'express';
const { router, app } = setupExpressApp();

let quoteStore = new Map<string, string>();

router.post('/v1/quote', async (req, res) => {
    await quoteStore.set(req.body.quote, req.body.quote)
    res.send(`Added ${req.body.quote}`)
});

router.get('/v1/quote-list', async (req, res) => {
  let quoteList = Array.from(await quoteStore.entries()).map(x => x[1])
  res.send(quoteList);
});

function setupExpressApp() {
  const app: any = express();
  const router = express.Router();
  let cors = require('cors');
  router.use(cors());
  router.use(express.urlencoded({ extended: true }));
  router.use(express.json());
  return { router, app };
}

app.listen(3000, async () => {
  console.log(`App listening locally`)
})

app.use('/', router)